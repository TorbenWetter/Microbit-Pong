import pygame
import utils


class Paddle(pygame.sprite.Sprite):
    def __init__(self, left_paddle, microbit):
        super().__init__()

        # initialize screen and set sizes
        self.area = pygame.display.get_surface()
        self.width = self.area.get_width()
        self.height = self.area.get_height()
        self.p_width = 10
        self.p_height = 100
        self.p_half_w = self.p_width / 2
        self.p_half_h = self.p_height / 2

        # set "image" for the paddle (left in red, right in blue)
        self.image = pygame.Surface([self.p_width, self.p_height])
        self.image.fill((255, 0, 0) if left_paddle else (0, 0, 255))
        self.rect = self.image.get_rect()

        # set object variables of the paddle
        self.microbit = microbit
        self.left_paddle = left_paddle

        # the paddle has to appear at the width of the belonging side and in the middle-height
        self.x = self.p_half_w if self.left_paddle else self.width - self.p_half_w
        self.y = self.height / 2

    def update(self):
        # the Micro:Bit's y value
        y_microbit = self.microbit.y

        # map the Micro:Bit's y value into the paddle's range to get the new y value
        # -2048 to 2048: possible y values from the Micro:Bit
        # -25 to 25: possible steps for the new y value
        steps = utils.map_n(y_microbit, -2048, 2048, -25, 25)

        # calculated new y value
        new_y = self.y + steps

        # min- and max- y values the paddle isn't allowed to pass (10px is the outer line wide)
        min_paddle = self.p_half_h + 10
        max_paddle = self.height - self.p_half_h - 10

        # set the interpolated y value from the Micro:Bit to the paddle's
        # it isnt's allowed to be below or above the paddle's range
        self.y = min_paddle if new_y < min_paddle else max_paddle if new_y > max_paddle else new_y

        # set updated x,y values to rect's x,y
        self.rect.x = self.x - self.p_half_w
        self.rect.y = self.y - self.p_half_h
