from microbit import *

while True:
    # register the serial port (Micro:Bit's default baudrate: 115200)
    uart.init(baudrate=115200)
    values = ','.join(map(str, accelerometer.get_values())) + '\r\n'
    # write the accelerometer values with uart
    uart.write(values)
    sleep(10)
