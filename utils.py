from scipy.interpolate import interp1d


# function to remap a number from one range to another
# noinspection PyTypeChecker
def map_n(n, start1, stop1, start2, stop2):
    interpolator = interp1d([start1, stop1], [start2, stop2])
    return float(interpolator(n))
