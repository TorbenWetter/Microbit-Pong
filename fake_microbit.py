import threading
from time import sleep


class FakeMicroBit:
    def __init__(self, left_paddle):
        self.x = self.y = self.z = 0
        self.microbit = left_paddle.microbit
        threading.Thread(target=self.update_acc_data).start()

    def update_acc_data(self):
        while True:
            self.x = self.microbit.y
            self.y = self.microbit.y
            self.z = self.microbit.y
            sleep(0.02)
