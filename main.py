import pygame
import math
import utils
from microbit import MicroBit
from fake_microbit import FakeMicroBit
from puck import Puck
from paddle import Paddle


# make the time nicer to look at
def time_string(n):
    s = str(n)
    return s if len(s) == 2 else "0" + s if len(s) == 1 else "00"


# initialize PyGame
pygame.init()

# hide mouse in window
pygame.mouse.set_visible(0)

# create the window (600x400) with a title
window = pygame.display.set_mode([600, 400])
pygame.display.set_caption('MicrobitPong')
# use the Poplar Std font
font = pygame.font.Font("poplar_std.ttf", 24)
# create the area to draw on
area = pygame.Surface(window.get_size())

# load the background image
background = pygame.image.load("background.png")
background = pygame.transform.scale(background, (area.get_width(), area.get_height()))

# load the score bar image
score_bar = pygame.image.load("score_bar.png")

# load the time bar image
time_bar = pygame.image.load("time_bar.png")

# load the ball image
ball = pygame.image.load("ball.png")

# initialize puck
puck = Puck()

# initialize the Micro:Bits as paddle (-controllers)

# default Micro:Bit path: /dev/ttyACM0 (Linux) | COM3 (Windows)
path = '/dev/ttyACM0'  # depending on the operating system

# left paddle is a must
left_paddle = Paddle(True, MicroBit(path))

# if you only have 1 Micro:Bit, use the FakeMicroBit
#right_paddle = Paddle(False, FakeMicroBit(left_paddle))

# if you have 2 Micro:Bits, initialize the second, too
# default path for a second Micro:Bit: /dev/ttyACM1 (Linux) | COM4 (Windows)
path2 = '/dev/ttyACM1'  # depending on the operating system
right_paddle = Paddle(False, MicroBit(path2))

elements = pygame.sprite.Group()
elements.add(puck)
elements.add(left_paddle)
elements.add(right_paddle)

# the game is ingame at the beginning. if it's over, nothing will move anymore
in_game = True

# the time will be over after one hour. nothing will move anymore then
time_over = False

# time units for the timer
milliseconds = seconds = minutes = 0

# set player scores to 0:0
scores = [0, 0]

# repeat drawing of the elements 120x/second
clock = pygame.time.Clock()
while in_game and not time_over:
    # draw background image
    window.blit(background, (0, 0))

    # update elements' positions
    puck.update()
    left_paddle.update()
    right_paddle.update()

    # if the puck hit's a paddle (if <(left) / >(right) x, if > y and if < y):
    #   set a new speed/direction for the puck depending on where the puck hits the paddle
    #   and set the puck's x to a "safe" location
    # -> left paddle
    if left_paddle.x <= puck.x - puck.puck_r <= left_paddle.x + left_paddle.p_half_w:
        if puck.y + puck.puck_r >= left_paddle.y - left_paddle.p_half_h:
            if puck.y - puck.puck_r <= left_paddle.y + left_paddle.p_half_h:
                # difference between the puck and the top of the paddle
                y_diff = puck.y - (left_paddle.y - left_paddle.p_half_h)
                # angle in which the puck will return from the paddle
                # pattern: http://zekechan.net/wp-content/uploads/2015/05/pong-02.png
                angle = utils.map_n(y_diff, -puck.puck_r, left_paddle.p_height + puck.puck_r, -math.radians(45),
                                    math.radians(45))
                puck.set_speed(angle)

                # set the puck's x to the right of the paddle so the puck isn't in the paddle anymore
                puck.x = left_paddle.x + left_paddle.p_half_w + puck.puck_r
    # -> right paddle
    if right_paddle.x >= puck.x + puck.puck_r >= right_paddle.x - right_paddle.p_half_w:
        if puck.y + puck.puck_r >= right_paddle.y - right_paddle.p_half_h:
            if puck.y - puck.puck_r <= right_paddle.y + right_paddle.p_half_h:
                # difference between the puck and the top of the paddle
                y_diff = puck.y - (right_paddle.y - right_paddle.p_half_h)
                # angle in which the puck will return from the paddle
                # pattern: http://zekechan.net/wp-content/uploads/2015/05/pong-02.png
                angle = utils.map_n(y_diff, -puck.puck_r, right_paddle.p_height + puck.puck_r, math.radians(225),
                                    math.radians(135))
                puck.set_speed(angle)

                # set the puck's x to the left of the paddle so the puck isn't in the paddle anymore
                puck.x = right_paddle.x - right_paddle.p_half_w - puck.puck_r

    # check if the puck is in the goal
    in_goal = puck.check_goal(scores)

    # update minutes/seconds for the timer
    if milliseconds > 1000:
        seconds += 1
        milliseconds -= 1000
    if seconds >= 60:
        minutes += 1
        seconds -= 60

    # draw ball image at the top-left corner of the puck
    window.blit(ball, (puck.x - puck.puck_r, puck.y - puck.puck_r))

    # draw score bar image in the middle at the top
    s_bar_x = area.get_width() / 2 - score_bar.get_width() / 2
    s_bar_y = area.get_height() / 15
    window.blit(score_bar, (s_bar_x, s_bar_y))

    # draw time bar image in the middle at the top below the score bar
    t_bar_x = area.get_width() / 2 - time_bar.get_width() / 2
    t_bar_y = s_bar_y + score_bar.get_height() - 3
    window.blit(time_bar, (t_bar_x, t_bar_y))

    # text color for the timer and the score (black | grey)
    score_color = (0, 0, 0)
    timer_color = (35, 35, 35)

    # draw the timer (minutes:seconds) as text
    window.blit(font.render("{}:{}".format(time_string(minutes), time_string(seconds)), 1, timer_color),
                (t_bar_x + 6, t_bar_y + 5))

    # draw players' points as text
    window.blit(font.render(str(scores[0]), 1, score_color), (s_bar_x + 60, s_bar_y + 5))
    window.blit(font.render(str(scores[1]), 1, score_color), (s_bar_x + 80, s_bar_y + 5))

    # draw elements on the window
    elements.draw(window)

    # update display
    pygame.display.flip()

    # if one player reaches 5 points, the game is over and in_game gets False
    if scores[0] >= 5 or scores[1] >= 5:
        in_game = False

    # after one hour the game is over and time_over gets True
    if minutes >= 60:
        time_over = True

    # wait a 120th of a second so the while loop runs 120x/second
    # add the milliseconds that passed since the last call
    milliseconds += clock.tick_busy_loop(120)

# if the game is over:

# gets the winning player
winner = 1 if scores[0] > scores[1] else 2

# draw a text at the end
win_color = (255, 215, 0)  # gold
win_x = area.get_width() / 2.75
win_y = area.get_height() - (area.get_height() / 7)

# the text is about the winning player or about time which is over
text = "Player {} won the game!".format(winner) if not in_game else "The time is over!"
window.blit(font.render(text, 1, win_color), (win_x, win_y))

# update display
pygame.display.flip()
