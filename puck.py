import pygame
import random
import math
import threading
from time import sleep


class Puck(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()

        # initialize screen and set sizes
        self.area = pygame.display.get_surface()
        self.width = self.area.get_width()
        self.height = self.area.get_height()
        self.puck_size = 20
        self.puck_r = self.puck_size / 2

        # set image for the puck (with a transparent background)
        self.image = pygame.Surface([self.puck_size, self.puck_size], pygame.SRCALPHA, 32)
        self.image = self.image.convert_alpha()

        # set rect for the puck with data from the image
        self.rect = self.image.get_rect()

        # set freezed to False, then 'at the start and at a goal'(self.reset()) set it to True
        self.freezed = False

        # set the start speed-multiplier to 2.5
        self.speed = 2.5

        # make the puck ready to start
        self.x = self.y = self.xspeed = self.yspeed = 0.0
        self.reset()

    def update(self):
        if not self.freezed:
            self.x += self.xspeed
            self.y += self.yspeed

        # prevent the puck getting outside the screen (10px is the outer line wide)
        if self.y < self.puck_r + 10 or self.y > self.height - self.puck_r - 10:
            self.yspeed *= -1

        # set updated x,y values to rect's x,y
        self.rect.x = self.x - self.puck_r
        self.rect.y = self.y - self.puck_r

        # increase the speed-multiplier at every update until it's at 5.0
        if not self.freezed and self.speed < 5.0:
            self.speed += 0.001

    # if a player couldn't keep the puck out of his goal add a point to one player and reset the puck
    def check_goal(self, scores):
        # point for player1
        if self.x > self.area.get_width() + self.puck_r:
            scores[0] += 1
        # point for player2
        elif self.x < -self.puck_r:
            scores[1] += 1
        else:
            # point for no player
            return False
        # point for a player
        self.reset()
        return True

    def freeze(self):
        self.freezed = True
        sleep(1)
        self.freezed = False

    def reset(self):
        # the puck has to appear in the middle
        self.x = self.width / 2
        self.y = self.height / 2

        # reset the speed-multiplier to 2.5
        self.speed = 2.5

        # freeze the puck for a second
        threading.Thread(target=self.freeze).start()

        # the puck should start in a random direction (with a random angle)
        start_angle = random.uniform(-math.pi / 4, math.pi / 4)
        self.set_speed(start_angle)

        # with a chance of 50% invert the direction
        if bool(random.getrandbits(1)):
            self.xspeed *= -1

    # sets the new x-/y- speed + direction (with the speed-multiplier)
    def set_speed(self, angle):
        self.xspeed = self.speed * math.cos(angle)
        self.yspeed = self.speed * math.sin(angle)
