import serial
import threading


class MicroBit:
    def __init__(self, device):
        # initialize accelerator variables
        self.x = self.y = self.z = 0

        # start listening for datas in new thread
        threading.Thread(target=self.update_acc_data, args=(device,)).start()

    def update_acc_data(self, device):
        """ register the serial port..
              default Micro:Bit path: /dev/ttyACM0
              default Micro:Bit baudrate: 115200 """
        microbit = serial.Serial(device, timeout=None, baudrate=115200)
        microbit.close()
        microbit.open()

        # no other program is allowed to use the serial port at the same time -> is the port open?
        if microbit.isOpen():
            try:
                while True:
                    # read bytes from Micro:Bit
                    line_in_bytes = microbit.readline()

                    # decode the string and replace 'new line characters'
                    line = line_in_bytes.decode(encoding='UTF-8').strip('\x00').rstrip()

                    try:
                        # put the values into a list
                        acc_values = [int(i) for i in line.split(',')]

                        # only use data if x, y and z are given and in a specific range
                        if len(acc_values) != 3 or not all(-2048 < i < 2048 for i in acc_values):
                            continue

                        # set global variables to new values
                        self.x, self.y, self.z = acc_values
                    except ValueError:
                        continue
            finally:
                microbit.close()
                print('An error occurred.')
        else:
            print('The serial port is already in use!')
